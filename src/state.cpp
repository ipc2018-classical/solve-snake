#include "state.h"

#include "globals.h"

#include <iostream>
#include <algorithm>
#include <cassert>

using namespace std;


State::State() :  to_spawn(g_num_ini_apples), pos_blocked(g_width*g_height, false) {
    for (int i = 0; i < to_spawn; ++i){
	apples.push_back(i);
	cout << "Initial state: " << g_apples[i] << endl;
    }
    assert (to_spawn <= (int)(g_apples.size()));

    for (const auto & pos : g_ini_snake) {
	snake.push_back(pos);
    }

    for (int pos : snake) {
	pos_blocked[pos] = true;
    }

}


bool State::is_applicable (int move) const {
    const auto & adj = neighbours(get_head());
    return move < adj.size() && !pos_blocked[adj[move]];
}

OperatorState State::apply_operator (int move) {
    int new_head = neighbours(get_head())[move];    

    int eaten_apple = NO_APPLE;
    for (auto it = apples.begin(); it < apples.end(); ++it) {
	if (g_apples[*it] == new_head) {
	    eaten_apple = *it;
	    apples.erase(it);
	    break;
	}
    }

    int tail_pos = snake.back();
    if(eaten_apple != NO_APPLE) {
	if (to_spawn < (int)g_apples.size()) {
	    apples.push_back(to_spawn++);
	}
    } else {
	pos_blocked[snake.back()] = false;
	snake.pop_back();
    }

    snake.insert(snake.begin(), new_head);
    pos_blocked[new_head] = true;

    return OperatorState(move, tail_pos, eaten_apple);
}

void State::undo(const OperatorState & op) {
    pos_blocked[get_head()] = false;
    snake.erase(snake.begin());

    if (op.eaten_apple != NO_APPLE) {
	if (apples.size() == g_num_ini_apples) {
	    apples.pop_back();
	    to_spawn --;
	}

	apples.insert( 
            std::upper_bound( apples.begin(), apples.end(), op.eaten_apple ),
            op.eaten_apple
        );
    } else {
	snake.push_back(op.pos_tail);
	pos_blocked[op.pos_tail] = true;
    }

}

// State::State(const State & other, const Pos & new_head) :
// 							  to_spawn(other.to_spawn),
// 							  snake(other.snake),
// 							  pos_blocked(other.pos_blocked) {
//     assert (to_spawn <= (int)(g_apples.size()));
//     int new_head_id = new_head.get_id();
//     int j = 0;
//     bool ate_apple = false;
//     for (int i = 0; i < MAX_APPLES; ++i) {
// 	assert(other.apples[i] <= g_width*g_height || other.apples[i] == NO_APPLE);
// 	if (other.apples[i] != NO_APPLE && new_head == g_apples[other.apples[i]]) {
// 	    ate_apple = true;
// 	} else {
// 	    apples[j++] = other.apples[i];
// 	}
//     }

//     for (; j < MAX_APPLES; ++j){
// 	if (to_spawn < (int)(g_apples.size())) {
// 	    apples[j] = to_spawn;
// 	    ++to_spawn;
// 	} else {
// 	    apples[j] = NO_APPLE;
// 	}
//     }
//     pos_blocked[new_head_id] = true;

//     snake.insert(snake.begin(), new_head);
//     if (!ate_apple) {
// 	pos_blocked[snake.back().get_id()] = false;
// 	snake.pop_back();
//     }

//     assert (to_spawn <= (int)(g_apples.size()));
// }

bool State::is_goal() const {
    return apples.empty();
}

  


ostream& operator<<(ostream& os, const State& s)  {
    os << "Snake: ";
    for (int asd : s.snake) {
	os << " " << xcoords(asd) << "-" << ycoords(asd);
    }
    os << endl;
    os << s.to_spawn << "/" << g_apples.size() << endl;
    for (int y = 0; y < g_height; ++y) {
	for (int x = 0; x < g_width; ++x) {
	    int id = pos_id(x, y);
	    if (s.pos_blocked[id]) {
		os << "*";
	    } else if (s.is_apple(id)) {
		os << "A";		
		//os << s.num_apple(id);
	    } else {
		os << " ";
	    }
	}
	os << endl;
    }
    return os;
}  

bool State::is_apple(int pos) const {
    for (int ap : apples) {
	if (g_apples[ap] == pos)
	    return true;
    }
    return false;
}


int State::num_apple(int pos) const {
    for (int ap : apples) {
	if (g_apples[ap] == pos)
	    return ap;
    }
    return -1;
}




State::State (const PackedStateBin *data) : pos_blocked(g_width*g_height, false) {
    apples.resize(*data);
    ++data;
    
    for (int i = 0; i < apples.size(); ++i) {
	apples[i] = *data;
        ++data;
     }

    if (apples.size() < g_num_ini_apples) {
	to_spawn = g_apples.size();
    } else {
	to_spawn = apples.back()+1;
    }

    snake.resize(*data);
    ++data;
    for (int i = 0; i < (int)(snake.size()); ++i) {
	snake[i] = *data;
        ++data;
	pos_blocked[snake[i]] = true;
     }
}
