#ifndef HEURISTIC_H
#define HEURISTIC_H


#include <vector>
class State;

class MinimumSpanningTreeHeuristic {
    std::vector<int> mst_precomputed;
public:
    MinimumSpanningTreeHeuristic();
    int compute(const State &state) const;
    int compute(const std::vector<int> & apples) const;
};


#endif
