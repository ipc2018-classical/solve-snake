#ifndef HEURISTIC_NOOBSTACLES_H
#define HEURISTIC_NOOBSTACLES_H

class State;
#include <unordered_map>
#include "heuristic.h"
#include <map>
#include <vector>

struct AbstractState {
    std::vector<int> apples;
    int snake_head;
public:
    AbstractState(const State & other);

    AbstractState(int head, std::vector<int> apples) : snake_head(head), apples(apples) {
    }

    AbstractState(const AbstractState & other, int eating_apple);
    void normalize();
    bool is_goal() const;

    int get_head() const {
	return snake_head;
    }

    int get_num_apples() const {
	return apples.size();
    }
    
    int get_cost_to_apple(int apple) const;

    bool operator==(const AbstractState &other) const {
	return (apples == other.apples && snake_head == other.snake_head);
    }

    bool operator!=(const AbstractState &other) const {
        return !(*this == other);
    }

    friend std::ostream& operator<<(std::ostream& os, const AbstractState& s);

    bool operator<(const AbstractState &other) const {
	if (snake_head < other.snake_head) {
	    return true;
	} else if (snake_head > other.snake_head) {
	    return false;
	}

	if (apples.size() < other.apples.size()) {
	    return true;
	} else if (apples.size() > other.apples.size()) {
	    return false;
	}

	for(int i = 0; i < apples.size(); ++i) {
	    if (apples[i] < other.apples[i]) {
		return true;
	    } else if (apples[i] > other.apples[i]) {
		return false;
	    }
	}
	return false;

    }

    std::size_t hash() const {
	std::size_t seed = apples.size() + 1;
	for(auto& i : apples) {
	    seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	}
	seed ^= snake_head + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	return seed;
    }
};


namespace std {

  template <>
  struct hash<AbstractState>
  {
    std::size_t operator()(const AbstractState & state) const
    {
	return state.hash();
    }
  };

}

class NoObstaclesHeuristic {
    std::map<AbstractState, int> cache; 

    MinimumSpanningTreeHeuristic mst_h;
public:
    int compute(const State &state);
    int compute(const AbstractState &state);
    int compute_approx(const AbstractState &state, int bound);
};

#endif
