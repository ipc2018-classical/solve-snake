#include "state_registry.h"

#include "globals.h"

using namespace std;

void state_to_packed_state(const State &state, PackedStateBin *data) {
    *data = state.apples.size();
    ++data;
    for (int i = 0; i < state.apples.size(); ++i) {
        *data = state.apples[i];
        ++data;
    }

    *data = state.snake.size();
    ++data;
    for (int pos : state.snake) {
        *data = pos;
        ++data;
    }
    
}

State packed_state_to_state(const PackedStateBin *data) {
    return State (data);

}

int compute_state_size() {
    return 3 + g_num_ini_apples + (2 + g_apples.size() + g_ini_snake.size());
}

StateRegistry::StateRegistry()
    : state_size(compute_state_size()),
      state_data_pool(state_size),
      registered_states(0,
                        StateIDSemanticHash(state_data_pool, state_size),
                        StateIDSemanticEqual(state_data_pool, state_size)) {
}

State StateRegistry::lookup_state(int id) {
    PackedStateBin *data = state_data_pool[id];
    return packed_state_to_state(data);
}

int StateRegistry::get_state_id(const State &state) {
    PackedStateBin *data = new PackedStateBin[state_size];
    state_to_packed_state(state, data);
    state_data_pool.push_back(data);

    StateID id = state_data_pool.size() - 1;
    pair<StateIDSet::iterator, bool> result = registered_states.insert(id);
    bool is_new_entry = result.second;
    if (!is_new_entry) {
        state_data_pool.pop_back();
    }
    assert(registered_states.size() == state_data_pool.size());
    return *result.first;
}
