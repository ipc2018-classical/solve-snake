#include "globals.h"
#include "state.h"
#include "ida_search.h"
#include "heuristic.h"
#include "heuristic_noobstacles.h"
#include "heuristic_obstacles.h"
#include "utils.h"

#include <iostream>
#include <fstream>  

using namespace std;

void print_pddl_operator (const State & s, const State & s2, std::ostream & outfile)  {

    if (!s.is_apple(s2.get_head())) {
	outfile << "(move " << Pos(s.get_head()) << " "
		  << Pos(s2.get_head()) << " " 
		  << Pos(s.get_tail()) << " "
		  << Pos(s2.get_tail()) << ")" <<std::endl;
	    
    } else if (s.get_to_spawn() == g_apples.size() ) {
	outfile << "(move-and-eat-no-spawn " << Pos(s.get_head()) << " " 
		  << Pos(s2.get_head())
		  << ")" <<  std::endl;
    } else if (s2.get_to_spawn() == g_apples.size()) {
	outfile << "(move-and-eat-spawn " << Pos(s.get_head()) << " "
		  << Pos(s2.get_head()) << " "
		  << Pos(g_apples[s.get_to_spawn()]) 
		  << " dummypoint)" <<  std::endl;
    } else {
	outfile << "(move-and-eat-spawn " << Pos(s.get_head()) << " "
		  << Pos(s2.get_head()) << " "
		  << Pos(g_apples[s.get_to_spawn()]) << " " 
		  << Pos(g_apples[s2.get_to_spawn()])
		  << ")" <<  std::endl;	
    }
}

int main(int argc, char** argv) {
    read_everything(cin);

    cout << *g_initial_state << endl;
    vector<OperatorState> plan;
    // if (argc == 2) {
    IDASearch<State, OperatorState, NoObstaclesHeuristic> engine;
    plan = engine.search();
    // } else {
    // 	IDASearch<State, OperatorState, ObstaclesHeuristic> engine;
    // 	plan = engine.search();
    // }

    if (plan.empty() && !g_initial_state->is_goal()) {
        cout << "no plan found" << endl;
    } else {
        cout << "plan found " << plan.size() << endl;
	if (argc == 2) {
	    cout << "Writing plan to: " << argv[1] << endl;
	    std::ofstream outfile (argv[1]);
	    State s (*g_initial_state);

	    for (const auto & op:  plan)  {
		State s2 = s;
		s2.apply_operator(op.id);
		print_pddl_operator (s, s2, outfile);
		s = s2;
	    }
	    outfile.close();	
	} else {
	    State s (*g_initial_state);
	    for (const auto & op:  plan)  {
		State s2 = s;
		s2.apply_operator(op.id);
		print_pddl_operator (s, s2, std::cout);
		s = s2;
		// cout << s << endl << endl;
	    }
	}
    }
    cout << "Peak memory: " << get_peak_memory_in_kb() << endl;

    return 0;
}
