#include "globals.h"

#include "state.h"
#include <ostream>
using namespace std;

int read_pos(std::istream &in) {
    int x,y;
    in >> x >> y;

    return pos_id(x, y);
}

void read_pos_vector(std::istream &in, std::vector<int> & posv) {
    int n;
    in >> n;
    posv.reserve(n);
    for (int i = 0; i < n; ++i) {
        posv.push_back(read_pos(in));
    }
}

void read_everything(std::istream &in) {    
    in >> g_width >> g_height;
    read_pos_vector(in, g_ini_snake);
    read_pos_vector(in, g_apples);
    g_num_ini_apples = g_apples.size();
    
    read_pos_vector(in, g_apples);
    g_initial_state = unique_ptr<State>(new State());

    g_apple_num.resize(num_positions(), -1);
    for (int i = 0; i < g_apples.size(); ++i) {
	g_apple_num[g_apples[i]] = i;
    }

    precomputed_adjacency.resize(num_positions());
    precomputed_distances.resize(num_positions()*num_positions());
    for (int x1 = 0; x1 < g_width; ++x1) {
	for (int y1 = 0; y1 < g_height; ++y1) {
	    int p1 = pos_id (x1, y1);
	    for (int x2 = 0; x2 < g_width; ++x2) {
		for (int y2 = 0; y2 < g_height; ++y2) {
		    int p2 = pos_id (x2, y2);
		    precomputed_distances[p1*num_positions() + p2] = abs(x1-x2) + abs(y1-y2);
		}
	    }

	    if (x1 > 0) {
		precomputed_adjacency[p1].push_back(pos_id(x1-1, y1));
	    }
	    if (x1+1 < g_width) {
		precomputed_adjacency[p1].push_back(pos_id(x1+1, y1));
	    }

	    if (y1 > 0) {
		precomputed_adjacency[p1].push_back(pos_id(x1, y1-1));
	    }
	    if (y1+1 < g_height) {
		precomputed_adjacency[p1].push_back(pos_id(x1, y1+1));
	    }
	}
    }
}

ostream& operator<<(ostream& os, const Timer& p) {
    return os <<  std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - p.start_time).count()  << "ms";
}

ostream& operator<<(ostream& os, const Pos& p) {
    return os << "pos" << xcoords(p.pos) << "-"  << ycoords(p.pos);
}


int g_width;
int g_height;
std::vector<int> g_apples;
std::vector<int> g_ini_snake;
int g_num_ini_apples;
std::vector<int> g_apple_num;

unique_ptr<State> g_initial_state;
Timer g_time;
std::vector<int> precomputed_distances;
std::vector<std::vector<int> > precomputed_adjacency;
