#ifndef IDA_SEARCH_H
#define IDA_SEARCH_H

#include "heuristic.h"
#include "open_list.h"
#include "per_state_information.h"
#include "state.h"
#include "state_registry.h"

#include <deque>
#include <vector>
#include "globals.h"

using Plan = std::vector<State>;

template <typename S, typename Operator, typename H> 
class IDASearch {
    H heuristic;
	
  public:
    IDASearch () {
    }

    std::vector<Operator> search() {
	std::vector<Operator> current_sequence;
	int g = 0;
	S current_state (*g_initial_state);
	int BOUND = heuristic.compute(current_state);    
	int excess  = std::numeric_limits<int>::max();
	std::cout << "Bound: " << BOUND <<  " " << g_time << std::endl;
	int op_id = 0;
	int num_states = 0;
	while (true) {
	    // cout << op_id << ": ";
	    if (op_id < Operator::num_operators) {
		if (current_state.is_applicable(op_id)) {
		    auto op = current_state.apply_operator(op_id);
		    num_states ++;
		    int h = heuristic.compute(current_state);
		    if (g + op.get_cost() + h  <= BOUND) {    
			current_sequence.push_back(op);
			if (h == 0 && current_state.is_goal()) {
			    return current_sequence;
			}
			op_id = 0;
			g += op.get_cost();
		    } else {
			excess = std::min (excess, g + op.get_cost() + h);
		    
			current_state.undo(op);
			op_id ++;
		    }
		} else {
		    op_id ++;
		}
	    } else { //Backtrack
		if (current_sequence.empty()) {
		    BOUND = excess;
		    excess = std::numeric_limits<int>::max();
		    std::cout << "Bound: " << BOUND  <<  " " << g_time <<  "  states: "  << num_states << std::endl;
		    num_states = 0;
		    op_id = 0;
		    g = 0;
		    assert(current_state == S(*g_initial_state));
		} else {
		    const Operator & op = current_sequence.back();
		    g -= op.get_cost();
		    op_id = op.id + 1;
		    current_state.undo(op);
		    current_sequence.pop_back();
		}	    
	    }	    
	}
    }
};

#endif
