#include "successors.h"

#include "globals.h"
#include <cassert>

using namespace std;


vector<State> get_successors(const State &state) {
    vector<State> successors;
    assert(state.to_spawn <= g_apples.size());
    const Pos & head = state.get_head();

    int id_right = head.id_right();
    if (id_right >= 0 && !state.is_obstacle(id_right)) {
	successors.push_back(State(state, head.right()));
    }

    int id_left = head.id_left();
    if (id_left >= 0 && !state.is_obstacle(id_left)) {
	successors.push_back(State(state, head.left()));
    }

    int id_up = head.id_up();
    if (id_up >= 0 && !state.is_obstacle(id_up)) {
	successors.push_back(State(state, head.up()));
    }
    
    int id_down = head.id_down();
    if (id_down >= 0 && !state.is_obstacle(id_down)) {
	successors.push_back(State(state, head.down()));
    }

    return successors;
}




