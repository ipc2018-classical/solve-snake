#ifndef SUCCESSORS_H
#define SUCCESSORS_H

#include "state.h"

#include <vector>
#include <iostream>

std::vector<State> get_successors(const State &state);

#endif
