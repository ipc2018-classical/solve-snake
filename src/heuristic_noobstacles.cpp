#include "heuristic_noobstacles.h"
#include "state.h"
#include "globals.h"

#include <iostream>
#include <unordered_map>
#include <cassert>

using namespace std;

int NoObstaclesHeuristic::compute(const State &state) {
    AbstractState as (state);
    as.normalize();
    
    return compute(as);
}

int NoObstaclesHeuristic::compute(const AbstractState &state) {
    if (state.is_goal()) {
	return 0;
    }
    if (cache.count(state)) {
	return cache[state];
    }

    int best = state.get_cost_to_apple(0) + compute(AbstractState(state, 0));
    
    for (int i = 1; i < state.apples.size(); ++i) {
	//int value = compute_approx(AbstractState(state, i), best - state.get_cost_to_apple(i));
	AbstractState succ(state, i);
	succ.normalize();
	int value = compute(succ);
	
	best = min(best, state.get_cost_to_apple(i) + value);
    }

    cache [state] = best;
    
    assert (best >= mst_h.compute(state.apples));
	    
    // cout << "Computed: " << state <<  ": " << best << endl;
    return best;
}


// int NoObstaclesHeuristic::compute_approx(const AbstractState &state, int bound) {
//     if (state.is_goal()) {
// 	return 0;
//     }
//     if (cache.count(state)) {
// 	return cache[state];
//     }

//     int hval = mst_h.compute(state.apples); 
//     if (hval >= bound) {
// 	return hval; 
//     }

//     int best = 10000;
//     for (int i = 0; i < state.apples.size(); ++i) {
// 	int cost = state.get_cost_to_apple(i);
// 	int h = compute_approx(AbstractState(state, i), min(best, bound-cost));
// 	best = min(best, cost + h);
//     }
 
//     //cache [state] = best;
//     return best;
// }

bool AbstractState::is_goal()const {
    return apples.empty();
}

AbstractState::AbstractState(const State & other) :
    apples(other.apples),
    snake_head(other.snake[0]) {
}

int AbstractState::get_cost_to_apple(int apple) const {
    return distance(snake_head, g_apples[apples[apple]]);
}

AbstractState::AbstractState(const AbstractState & other, int eating_apple) :
    apples(other.apples), snake_head(g_apples[apples[eating_apple]]) {
    
    int to_spawn = apples.back() + 1; 
    apples.erase(apples.begin() + eating_apple);
    if (apples.size() == g_num_ini_apples -1 && to_spawn < g_apples.size()) {
	apples.push_back(to_spawn);
    } 
}

void AbstractState::normalize() {
    while(apples.size() == g_num_ini_apples && apples.back() - apples[0] > 30) {
    	int to_spawn = apples.back() + 1; 
    	apples.erase(apples.begin());
    	if (apples.size() == g_num_ini_apples -1 && to_spawn < g_apples.size()) {
    	    apples.push_back(to_spawn);
    	}
    }


    // if (apples.size() == g_num_ini_apples && apples.back() - apples[0] > 30){
    // 	for (int i = 0; i+1 < apples.size(); i++) {
    // 	    if (apples[i+1] - apples[i] > 16) {
    // 		int to_spawn = apples.back() + 1; 

    // 		apples.erase(apples.begin() + i);
    // 		if (apples.size() == g_num_ini_apples -1 && to_spawn < g_apples.size()) {
    // 		    apples.push_back(to_spawn);
    // 		}
    // 		i = 0;
    // 	    }
    // 	}
    // }
}



ostream& operator<<(ostream& os, const AbstractState& s)  {
    os << s.snake_head << " ";
    for (int apple : s.apples) {
	os << " " << apple;
    }
    return os;
}  
