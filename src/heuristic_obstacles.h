#ifndef HEURISTIC_OBSTACLES_H
#define HEURISTIC_OBSTACLES_H

#include "heuristic_noobstacles.h"

#include <boost/dynamic_bitset.hpp>


struct Possibility {
    boost::dynamic_bitset<> blocked_surely;
    // boost::dynamic_bitset<> maybe_visited;
    boost::dynamic_bitset<> remaining_apples;
    int to_spawn;
    
    Possibility();
    
    void update (int head, const Possibility & other);

    void reset (int head, const Possibility & other) ;

    void reset (int head,
		const boost::dynamic_bitset<> & remaining_apples_,
		int to_spawn);
    
    bool is_goal() const {
	return remaining_apples.empty();
    }

};


class ObstaclesHeuristic {
    NoObstaclesHeuristic mst_h;

    std::vector<Possibility> possibilities_by_head;
    std::vector<Possibility> new_possibilities_by_head;
    //std::vector<Possibility> new_apple_possibilities_by_head;

    boost::dynamic_bitset<> is_new_head;
    std::vector<int> heads;
    std::vector<int> new_heads;
    


public:
    ObstaclesHeuristic();
    int compute(const State &state);
};

#endif
