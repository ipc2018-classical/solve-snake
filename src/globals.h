#ifndef GLOBALS_H
#define GLOBALS_H

#include <chrono>

#include <ostream>

#include <iostream>
#include <memory>
#include <vector>

class Timer {
    std::chrono::steady_clock::time_point start_time;

public:
Timer() : start_time(std::chrono::steady_clock::now()) {}
    
    friend std::ostream& operator<<(std::ostream& os, const Timer& p);
};

class State;

void read_everything(std::istream &in);

extern int g_width;
extern int g_height;
extern std::vector<int> g_apples;
extern std::vector<int> g_ini_snake;
extern int g_num_ini_apples;

extern std::vector<int> g_apple_num;


extern std::unique_ptr<State> g_initial_state;
extern Timer g_time;
extern std::vector<int> precomputed_distances;
extern std::vector<std::vector<int> > precomputed_adjacency;


inline int num_positions() {
    return g_height*g_width;
}

inline int pos_id(int x, int y) {
    return x + y*g_width;
}

inline int xcoords(int pos) {
    return pos % g_width;
}

inline int ycoords(int pos) {
    return (pos - xcoords(pos))/g_width;
}
inline const std::vector<int> & neighbours(int p1) {
    return precomputed_adjacency[p1];
}

inline int distance(int p1, int p2) {
    return precomputed_distances[p1*num_positions() + p2];
}


class Pos {
    int pos;
public:
    Pos (int p) : pos(p) {}

    friend std::ostream& operator<<(std::ostream& os, const Pos& p);
};



#endif
