#include "search.h"

#include "globals.h"
#include "successors.h"
#include "open_list.h"
#include "heuristic_noobstacles.h"
#include "state.h"
#include "heuristic_obstacles.h"

#include <algorithm>
#include <cassert>

using namespace std;

template <typename T, typename H> 
Plan SearchEngine<T, H>::search(int bound, float w) {
    assert(w >= 1);
    int initial_state_id = registry.get_state_id(*g_initial_state);
    open.insert(initial_state_id, *g_initial_state, 0, heuristic.compute(*g_initial_state));
    SearchNode &node = search_space[initial_state_id];
    node.parent_state_id = initial_state_id;
    node.g = 0;
    node.status = SearchNodeStatus::OPEN;

    while (!open.empty()) {
        StateID s_id = open.pop_min();
        SearchNode &node = search_space[s_id];

        if (node.status == SearchNodeStatus::CLOSED) {
            continue;
        }
        node.status = SearchNodeStatus::CLOSED;
        State s = registry.lookup_state(s_id);
        if (s.is_goal()) {
            return extract_solution(s_id);
        }

        for (int op_id = 0; op_id < OperatorState::num_operators; ++op_id) {
	    if (!s.is_applicable(op_id)) continue;
	    State succ (s);
	    succ.apply_operator(op_id);
	    int hval = heuristic.compute(succ);
	    if (hval == DEAD_END) {
		continue;
	    }
	    int succ_id = registry.get_state_id(succ);
            SearchNode &succ_node = search_space[succ_id];

	    if (node.g + 1 + hval > bound) continue;
	    
            if (succ_node.status == SearchNodeStatus::NEW) {
                succ_node.status = SearchNodeStatus::OPEN;
                succ_node.g = node.g + 1;
                succ_node.parent_state_id = s_id;

                open.insert(succ_id, succ, succ_node.g, (int)(hval*w));
            } else if (succ_node.g > node.g + 1) {
                succ_node.status = SearchNodeStatus::OPEN;
                succ_node.g = node.g + 1;
                succ_node.parent_state_id = s_id;

		open.insert (succ_id, succ,  succ_node.g, (int)(hval*w));
            }

	    static int best_heuristic = -1;

	    if (best_heuristic == -1 || hval < best_heuristic) {
		cout << "new best heuristic value: " << hval
		     << " with g value " << succ_node.g << endl;
		//cout << succ << endl;
		best_heuristic = hval;
	    }

        }
    }
    return Plan();
}

template <typename T, typename H> 
Plan SearchEngine<T, H>::extract_solution(int s_id) {
    //cout << registry.size() << endl;
    Plan plan;
    while (s_id != search_space[s_id].parent_state_id) {
        plan.push_back(registry.lookup_state(s_id));
        s_id = search_space[s_id].parent_state_id;
    }
    plan.push_back(registry.lookup_state(s_id));
    reverse(plan.begin(), plan.end());
    return plan;
}

void x() {
    SearchEngine<OpenList, NoObstaclesHeuristic> s;
    s.search(0, 0);
    SearchEngine<OpenList, MinimumSpanningTreeHeuristic> s2;
    s2.search(0, 0);
    SearchEngine<OpenList, ObstaclesHeuristic> s3;
    s3.search(0, 0);
}
