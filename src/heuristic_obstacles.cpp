
#include "heuristic_obstacles.h"
#include "heuristic_noobstacles.h"

#include "globals.h"
#include "state.h"
#include <cassert>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <boost/dynamic_bitset.hpp>

using namespace std;


ObstaclesHeuristic::ObstaclesHeuristic() :
    possibilities_by_head (num_positions()), new_possibilities_by_head (num_positions()), is_new_head (num_positions()) {

    heads.reserve(num_positions());
    new_heads.reserve(num_positions());
}


Possibility::Possibility() : blocked_surely (g_width*g_height, false),
			     remaining_apples (g_apples.size(), true), to_spawn(-1) {
}


void Possibility::update (int head, const Possibility & other) {
    blocked_surely &= other.blocked_surely;
    //maybe_visited |= other.maybe_visited;
		    
    blocked_surely.set(head);

    if (g_apple_num[head] >= 0 && remaining_apples [g_apple_num[head]] && to_spawn >= g_apple_num[head]) {
	remaining_apples.reset(g_apple_num[head]);
	to_spawn ++;
    }
}
void Possibility::reset (int head, const Possibility & other) {
    blocked_surely = other.blocked_surely;
    //maybe_visited = other.maybe_visited;
    remaining_apples = other.remaining_apples;
    to_spawn = other.to_spawn;	    
    //maybe_visited.set(head);
		    
    blocked_surely.set(head);

    if (g_apple_num[head] >= 0 && remaining_apples [g_apple_num[head]] && to_spawn >= g_apple_num[head]) {
	remaining_apples.reset(g_apple_num[head]);
	to_spawn ++;
    }
}

    void Possibility::reset (int head,
		const boost::dynamic_bitset<> & remaining_apples_,
		int to_spawn) {
	//maybe_visited (g_width*g_height, false),
	blocked_surely.reset();
	remaining_apples = remaining_apples;
	to_spawn = to_spawn;
	blocked_surely.set(head);
	//maybe_visited.set(head);
    }


int ObstaclesHeuristic::compute(const State &state) {
    int head_id = state.get_head();    
    
    boost::dynamic_bitset<> remaining_apples_ini (g_apples.size(), false);
    for (int apple : state.apples) {
	remaining_apples_ini.set(apple);
    }
    for(int i = state.to_spawn; i < (int)g_apples.size(); ++i) {
	remaining_apples_ini.set(i);
    }
    
    heads.clear();
    new_heads.clear();
    
    possibilities_by_head[head_id].reset(state.get_head(), remaining_apples_ini, state.to_spawn);
    heads.push_back(head_id);
    auto blocked = state.get_blocked();
    int num_steps = 0;
    int best_possible_outcome = DEAD_END;
    while (!possibilities_by_head.empty() && num_steps < (int)state.snake.size()) {
	blocked [state.snake[state.snake.size() - num_steps - 1]] = false;
	for (int head : heads) {
	    Possibility &pos = possibilities_by_head[head];
	    if (pos.is_goal()) {
		return min(best_possible_outcome, num_steps);
	    }

	    if (num_steps  >= (int)state.snake.size() - 1) {
		std::vector<int> apples_h;
		int i = 0;
		for(int count = 0;
		    count < g_num_ini_apples &&  i < pos.remaining_apples.size();
		    ++i) {
		    if (pos.remaining_apples.test(i)) {
			++count;
			apples_h.push_back(i);
		    }
		}
		if (apples_h.size() ==  g_num_ini_apples) {
		    for ( int j = pos.remaining_apples.size() -1; j > i; --j) {
			if (!(pos.remaining_apples.test(j))) {
			    apples_h.pop_back();			
			    if(j+1 < g_apples.size()) {
				apples_h.push_back(j+1);
			    } 
			}
		    }
		}
		//cout << pos.remaining_apples << endl;
		//cout << "x: " << AbstractState(head, apples_h) << endl;
		best_possible_outcome = min(best_possible_outcome, num_steps + mst_h.compute(AbstractState(head, apples_h)));
	    } else {
		//pos.maybe_to_blocked(num_steps);
		for (int new_head : neighbours(head)) {
		    if (!pos.blocked_surely[new_head] && !blocked[new_head]) {
			if (!is_new_head[new_head]) {
			     
			    new_heads.push_back(new_head);
			    is_new_head.set(new_head);
			    new_possibilities_by_head[new_head].reset(new_head, pos);
			} else {
			    new_possibilities_by_head[new_head].update(new_head, pos);
			}
		    }
		}
	    }
	}
    
	new_possibilities_by_head.swap(possibilities_by_head);
	is_new_head.reset();
	heads.swap(new_heads);
	new_heads.clear();
	num_steps ++;
    }

    return best_possible_outcome;
}
