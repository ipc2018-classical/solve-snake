#include "heuristic.h"

#include <vector>
#include "globals.h"
#include "state.h"

#include <map>

using namespace std;


int apple_distance(int a1, int a2) {
    return distance(g_apples[a1], g_apples[a2]);
}

int precompute_mst(int num_positions, const map<int, vector<pair<int, int>>> & sorted_edges) {
    vector<int> set_of_pos (num_positions);
    for (int i = 0; i < num_positions; ++i) {
	set_of_pos [i] = i;
    }

    int res = 0;
    int remaining_edges = num_positions-1;
    
    for (const auto & entry : sorted_edges) {
	for (const auto & edge : entry.second) {
	    assert(edge.first < set_of_pos.size());
	    assert(edge.second < set_of_pos.size());
	    if (set_of_pos[edge.first] != set_of_pos[edge.second]) {
		res += entry.first;
		int set1 = set_of_pos[edge.first]; 
		int set2 = set_of_pos[edge.second];
		for (auto & sop : set_of_pos) {
		    if (sop == set1) {
			sop = set2;
		    }
		}
		if (--remaining_edges == 0) {
		    return res;
		}
	    }
	}
    }
    return res;    
}

MinimumSpanningTreeHeuristic::MinimumSpanningTreeHeuristic() {
    map<int, vector<pair<int, int>>> edges_by_cost;
    for (int i = 0; i < (int)g_apples.size(); ++i) {
	int apple_i = (int)g_apples.size()-1-i;	
	for (int j = 0; j < i; ++j)  {
	    int apple_j = (int)g_apples.size()-1-j;
	    int cost = apple_distance(apple_i, apple_j);
	    edges_by_cost[cost].push_back(make_pair(i, j));
	}
	mst_precomputed.push_back(precompute_mst(i+1, edges_by_cost));
    }
}

int MinimumSpanningTreeHeuristic::compute(const State &state) const {
    return std::max((int)state.apples.size() + (int)g_apples.size() - state.to_spawn,
	       mst_precomputed [(int)g_apples.size() - state.to_spawn]);
}


int MinimumSpanningTreeHeuristic::compute(const vector<int> & apples) const {
    int i = apples.size() - 1;
    int to_spawn = apples.size() == g_num_ini_apples ? apples[i] + 1 : g_apples.size();
    while (i > 0 && apples[i] == apples[i-1]+1) {
	i--;
    }
     
    return std::max((int)apples.size() + (int)g_apples.size() - to_spawn,
		    mst_precomputed [(int)g_apples.size() - to_spawn]
	);
}

