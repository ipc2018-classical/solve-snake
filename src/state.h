#ifndef STATE_H
#define STATE_H

#include <cstdint>
#include <vector>
#include <array>
#include <iostream>
#include <deque>
#include <list>

#include <cassert>

/* static const int MAX_APPLES = 5; */
static const int NO_APPLE = 10000;
static const int DEAD_END = 1000000;

using StateID = int;

struct OperatorState {
    int id;
    int pos_tail;
    int eaten_apple;

    const static int num_operators = 4;
    OperatorState (int id, int pos_tail, int eaten_apple) : id (id), pos_tail(pos_tail), eaten_apple(eaten_apple) {}

    inline int get_cost () const {
	return 1;
    }
};

using PackedStateBin = uint16_t;

struct State {
    int to_spawn;
    std::deque<int> snake;
    std::vector<int> apples;
    std::vector<bool> pos_blocked;
    
public:

    State(const State & other) = default;
    
    State();

    State (const PackedStateBin *data);


    bool is_applicable(int num_operator) const;
    OperatorState apply_operator(int num_operator);
    void undo(const OperatorState & op);

    bool is_goal() const;
    
    bool is_obstacle(int id_pos) const {
	assert (id_pos >= 0);
	assert (id_pos < (int) (pos_blocked.size()));
	return pos_blocked[id_pos];
    }

    const std::vector<bool> & get_blocked () const {
	return pos_blocked;
    }
    bool is_apple(int pos) const ;
    int num_apple(int pos) const ;
    
    bool operator==(const State &other) const {
	return (apples == other.apples && snake == other.snake);
    }

    bool operator!=(const State &other) const {
        return !(*this == other);
    }

    int get_head() const {
	return snake.front();
    }

    int get_to_spawn() const {
	return to_spawn;
    }

    int get_tail() const {
	return snake.back();
    }

    friend std::ostream& operator<<(std::ostream& os, const State& c) ;
};






#endif
