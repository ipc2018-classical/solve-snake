#include "globals.h"
#include "state.h"
#include "ida_search.h"
#include "heuristic.h"
#include "heuristic_noobstacles.h"
#include "heuristic_obstacles.h"
#include "utils.h"
#include "search.h"

#include <iostream>
#include <fstream>  

using namespace std;

void print_pddl_operator (const State & s, const State & s2, std::ostream & outfile)  {

    if (!s.is_apple(s2.get_head())) {
	outfile << "(move " << Pos(s.get_head()) << " "
		  << Pos(s2.get_head()) << " " 
		  << Pos(s.get_tail()) << " "
		  << Pos(s2.get_tail()) << ")" <<std::endl;
	    
    } else if (s.get_to_spawn() == g_apples.size() ) {
	outfile << "(move-and-eat-no-spawn " << Pos(s.get_head()) << " " 
		  << Pos(s2.get_head())
		  << ")" <<  std::endl;
    } else if (s2.get_to_spawn() == g_apples.size()) {
	outfile << "(move-and-eat-spawn " << Pos(s.get_head()) << " "
		  << Pos(s2.get_head()) << " "
		  << Pos(g_apples[s.get_to_spawn()]) 
		  << " dummypoint)" <<  std::endl;
    } else {
	outfile << "(move-and-eat-spawn " << Pos(s.get_head()) << " "
		  << Pos(s2.get_head()) << " "
		  << Pos(g_apples[s.get_to_spawn()]) << " " 
		  << Pos(g_apples[s2.get_to_spawn()])
		  << ")" <<  std::endl;	
    }
}

int main(int argc, char** argv) {
    read_everything(cin);

    int bound = 100000;
    int weight = 10000;
    if (argc >= 3) {
	bound = atoi(argv[2]);
    }
    if (argc >= 2) {
	weight = atoi(argv[1]);
    }

    cout << *g_initial_state << endl;
    vector<State> plan;
    SearchEngine<OpenList, NoObstaclesHeuristic> engine;
    plan = engine.search(bound, weight);
    while (true) {
	if(plan.empty()) {
	    cout << "no plan found" << endl;
	    return 0;
	}
	cout << "plan found " << plan.size() -1 << endl;
	bound = plan.size() - 2;
	if (argc >= 4) {
	    cout << "Writing plan to: " << argv[3] << endl;
	    std::ofstream outfile (argv[3]);
	    

	    for (size_t i = 1; i < plan.size(); ++i)  {
		print_pddl_operator (plan[i-1], plan[i], outfile);
	    }
	    outfile.close();	
	}
	
	const auto plan2 =  engine.search(bound, weight);
	if (plan2.empty()) {
	    continue;
	}
	if (plan2.size() < plan.size()) {
	    plan = plan2;
	}	
	cout << "Peak memory: " << get_peak_memory_in_kb() << endl;
    }



    return 0;
}
